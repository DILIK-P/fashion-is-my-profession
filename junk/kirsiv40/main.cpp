#include "visualizer.h"

#include <QThread>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QLabel>
#include <QtWidgets/QStylePainter>
#include <QTextEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QCheckBox>
#include <QRadioButton>

constexpr int w = 369;
constexpr int h = 800;

int main(int argc, char** argv) {
    QApplication app(argc, argv);

    Screen* scr = new Screen;
    scr->resize(w, h);

    scr->SetUp();

    scr->show();

    return app.exec();
}
