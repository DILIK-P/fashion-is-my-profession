#ifndef OUTFIT_H
#define OUTFIT_H

#include <QString>
#include <QVector>

class Outfit
{
public:
    Outfit() = default;

    double getDressability() const;
    void setDressability(double newDressability);

    double getQuality() const;
    void setQuality(double newQuality);

    QString getName() const;
    void setName(const QString &newName);

    static QVector<Outfit> collection;

private:
    QString name = "";
    double dressability = 0;
    double quality = 0;
};

#endif // OUTFIT_H
