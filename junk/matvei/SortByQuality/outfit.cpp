#include "outfit.h"


double Outfit::getDressability() const
{
    return dressability;
}

void Outfit::setProbability(double newDressability)
{
    probability = newDressability;
}

double Outfit::getQuality() const
{
    return quality;
}

void Outfit::setQuality(double newQuality)
{
    quality = newQuality;
}

QString Outfit::getName() const
{
    return name;
}

void Outfit::setName(const QString &newName)
{
    name = newName;
}
